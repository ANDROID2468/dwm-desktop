#! /bin/bash
echo "DWM Desktop"
echo "BY ANDROID2468"
echo ""
input="${1}"

make_install () {
     	echo "building dwm... " 
	cd dwm
	sudo make install
        make clean
	cd ..	
	echo "building dmenu..."
	cd dmenu
	sudo make install
	make clean
	cd ..
	echo "building slstatus"
	cd slstatus
	sudo make install
	make clean
	cd ..
	echo "building st"
	cd st
	sudo make install
	make clean
	cd ..
		}

make_uninstall () {
     	echo "uninstalling dwm... " 
	cd dwm
	sudo make uninstall
        cd ..	
	echo "uninstalling dmenu..."
	cd dmenu
	sudo make uninstall
	cd ..
	echo "uninstalling slstatus"
	cd slstatus
	sudo make uninstall
	cd ..
	echo "uninstalling st"
	cd st
	cd ..
		}


if [ "${input}" = "--uninstall" ];
then
	    make_uninstall
	    rm -rf -v ~/.local/share/dwm
	    rm -rf -v  ~/.local/share/systemd/user/slstatus.service
	    rm -rf -v  ~/.xprofile
	    echo "Done!"
	    exit

fi


if [ ! -f ~/.local/share/dwm/wallpaper.jpg ]
then
	    echo "DWM not installed, doing clean install"
	    make_install
	    mkdir -v -p ~/.local/share/dwm
	    mkdir -v -p ~/.local/share/systemd/user
	    cp -v wallpaper.jpg ~/.local/share/dwm
	    cp -v slstatus.service ~/.local/share/systemd/user/slstatus.service
	    cp -v .xprofile ~/.xprofile

    else
	   echo "DWM installed, reinstalling"
	   make_install
fi


